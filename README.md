Docker Control Plane
====================

The purpose of this project is to provide control plan capabilities around the docker remote API that can opperate in a distributed manner over multiple physical / virtual hosts as long as they are running the docker-control-plane agent.

The original concept of this project has evolved so that this aspect of the project is around policy and features will be added to a fork of docker (that will hopefully be merged into core) to support the control plane.

_In order to stay with the "shipping" theme of docker I have been attempting to come up with a good name of the project. So far I have only hit upon **pilot**, and I am not sold on it yet, so if you have an alternate please let me know._

![Conceptual Overview](https://bitbucket.org/ciena/docker-control-plane/raw/664c2e7fe8c9093cd0a6ed9e67aff033df9aad43/conceptual.png)

Basic Concepts
==============

Docker Daemon
-------------
This is the standard docker daemon that will be required on each host. In order to support the control plane the follow modifications will be made to a fork of the docker daemon. The purpose of these changes are really to eliminate the need for a shadow daemon or central repository to maintain information about the docker daemons. By putting these capabilities directly into the docker daemon the storage is natrually distributed without the need for a a centralized, and perhaps syncrhronized, store.

+ Tags - the docker daemon will maintain a list of tags that have been assigned to it. This list of tags can be set via the command line when the daemon is started or in the daemon configuration. The set of tags should be able to be manipulated via the daemons API.
+ Periodic BOM Broadcast - at a configurable interval the daemon should broadcast its manifest on a defined UDP port. Standard networking technologies can be used to bridge this broadcast across LAN segments. The manifest should include the following:
    + host identifier (likely the IP)
    + API URI - the URI on which the daemon listens for API connections
    + tags - the current set of tags associated with the daemon
    + health metric - some characterization of the load / health of the docker deaemon [specifics TBD]
    + container manifest - the list of containers and their parameters that are currently running. Including the image identifier.

Docker Image Repository
-----------------------------------
Standard docker capabilities.

Pilot Daemon
-------------------
The basic concept is to have a small daemon deployed that will listen in on the BOM messages from the docker daemons and apply policy to the data. This policy may include balancing the load, starting/stopping container instances, etc.

Because the pilot will be evaluating policy globally and because it is desirable to have the pilot scale as well as be highly available, there will have to be some level of synchronization between pilot instances so that policy isn't double evaluated causing double the new instances of containers to be spun up for example. This synchronization should be implemented via messaging if possible as opposed to shared storage.

It is possible that the changes to the docker deamon proposed above are not accepted in the master stream of docker. If this is the case it is possible the the pilot takes on these responsiblities for the docker daemons.

It is possible, and perhaps desirable, the the pilot is simply a container instance running in docker.

API
---
The API will be consitent with the current docker API. To abstract the service from the client technologies such as HTTP load balancing can be deployed.

Presentation
------------
A web based presentation will be provided. This presentation layer will leverage the API for all capabilities. There currently is a web presentation layer to manually control docker called shipyard (https://github.com/shipyard/shipyard). This presentation layer has not been evaluated, but it may be appropriate to extend this presentation / control layer to support pilot.

Policy
------
Control plane decisions will be influenced by a policy base. This policy base will determine actions based on events and control messages recieved from control plane agents, container instances, and the API.

This is essentially a rule base and should be "live" in that there is a state associated with it and that state changes over time as additional data is added to the knowledge base. Based on the data, actions may be triggered.

